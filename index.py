from flask import Flask, render_template, request
import requests


from flask import Flask
app = Flask(__name__)

perro_api = "https://dog.ceo/api/breeds/image/random"


@app.route('/')
def index():
    r = requests.get(perro_api).json()
    rope = r['message']
    if request.method == 'GET':
        return render_template('index.html', rope=rope)
    else:
        return render_template('index.html', rope=rope)


if __name__ == '__main__':
    app.run()
